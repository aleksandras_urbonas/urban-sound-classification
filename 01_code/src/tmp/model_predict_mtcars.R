mtcars$cyl <- factor(mtcars$cyl)

idx_train <-
  caret::createDataPartition(
    y=mtcars$cyl
    , p=0.75
    , list=F
  )

train_data = mtcars[ idx_train,]
test_data = mtcars[-idx_train,]


modelRF <-
  caret::train(
    factor(cyl) ~ .
    , method="rf"
    , data=train_data
  )

p <-
  predict(
    modelRF
    , test_data
    , type = "raw"
  )

## accuracy (overall)
cm$overall[1]
