# Title: remove-null.R
# Version: 2020-05-05T2158 AU

length(idx_0 <- which(snd == 0))
if(length(idx_0) > 0) {

  # exclude
  data <- data[-idx_0]

  # recreate
  snd <- data@left
  
  # notify
  cat('! zeros removed:', length(idx_0), '\n')
}
rm(idx_0)
