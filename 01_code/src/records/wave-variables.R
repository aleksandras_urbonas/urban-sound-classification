# Title: wave-variables.R
# Author: Aleksandras Urbonas
# Version: 2020-05-05T2007 AU


samp.rate <- this_wave@samp.rate
dt <- 1024 / samp.rate # delta time
window.size <- dt # window size
hop.size <- window.size # hop size
samp.N <- window.size * samp.rate # sample count
slices <- round(length(this_wave@left)/ samp.N, 0)
