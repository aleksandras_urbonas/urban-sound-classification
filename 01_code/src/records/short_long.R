# Title: short_long.R
# Author: Aleksandras Urbonas
# Version: 2020-05-05T2300 AU


if(length(snd) < max_samples) {
  cat('short\n\n')
  records$qc[record] <- 'short'
} else if(length(snd) > max_samples) {
  records$size_magnitude[record] <- round(length(snd) / max_samples)
  cat('longer', records$size_magnitude[record],'times \n\n')
  records$qc[record] <- 'long'
}