# https://stackoverflow.com/questions/42379138/keras-valueerror-no-data-provided-for-input-1-need-data-for-each-key
library(keras)

#The network should identify the rule that a row sum greater than 1.5 should yield an output of 1


my_x=as.matrix(X, ncol=dim(X)[1], nrow=dim(X)[2])
# str(my_x)

# my_y=ifelse(rowSums(my_x)>1.5,1,0)
# my_y=to_categorical(my_y, 2)
str(my_y)
my_y=as.matrix(Y)


model = keras_model_sequential()

layer_dense(model,units = 2000, activation = "relu", input_shape = c(1016))
layer_dropout(model,rate = 0.4)
layer_dense(model,units = 50, activation = "relu")
layer_dropout(model,rate = 0.3)
layer_dense(model,units = ncol(my_y), activation = "softmax")

compile(model,loss = "categorical_crossentropy",optimizer = optimizer_rmsprop(),metrics = c("accuracy"))

history <-
  fit(
    model
    , my_x
    , my_y
    , epochs = 5
    , batch_size = 128
    , validation_split = 0.2
    )

evaluate(model,my_x, my_y,verbose = 0)

predict_classes(model, my_x)
