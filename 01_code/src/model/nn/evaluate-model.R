# evaluate-model.R

# Here we plot loss and accuracy metrics using the returned model object
plot(history)


evaluate(model, my_x, my_y, verbose = 0)


# Evaluate the model’s performance on the test data:
model %>% evaluate(my_x, my_y)

# Evaluate the model’s performance on the valid data:
model %>% evaluate(my_x_valid, my_y_valid)


# Predictions on test data
p <- predict_classes(model, my_x)

# Predictions summary
table(p)


#### Confusion Matrix ####
# cm <-
#   caret::confusionMatrix(
#     factor(my_y)
#     , factor(p)
#   )

## accuracy (overall)
# cm$overall[1]
