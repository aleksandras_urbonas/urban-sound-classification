## Libraries
library('keras')


## first time run
# install_keras()


## Read in MNIST data
mnist <- keras::dataset_mnist()


## Split train and test
x_train <- mnist$train$x
y_train <- mnist$train$y

x_test <- mnist$test$x
y_test <- mnist$test$y


## reshape: rows x columns
x_train <- array_reshape(x_train, c(nrow(x_train), 784))
x_test <- array_reshape(x_test, c(nrow(x_test), 784))


## rescale
x_train <- x_train / 255
x_test <- x_test / 255


## prepare outcome (one-hot for class)
# y_train <- to_categorical(y_train, 10)
# y_test <- to_categorical(y_test, 10)

